﻿using NumericalIntegration.Model.OrdinaryDifferentialEquation;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NumericalIntegration.Model.NumericalMethods
{
	internal class AdamsMethod : NumericalMethod
	{
		internal override List<DataPoint> Calculate(string func, IOrdinaryDifferentialEquation equation, double endX, double step)
		{
			List<DataPoint> resPoints = new List<DataPoint>();

			double startX = equation.InitialConditions[0].X;

			List<double> funcValues = new List<double>();

			// установка начальной точки
			resPoints.Add(new DataPoint(startX, equation.InitialConditions[0].Y));

			// создание копии ограничений
			IOrdinaryDifferentialEquation equationCopy = equation.Clone() as IOrdinaryDifferentialEquation;

			for (double x = startX; x < endX; x += step)
			{
				// изменение ограничений
				equationCopy.InitialConditions[0].X = resPoints[resPoints.Count - 1].X;
				equationCopy.InitialConditions[0].Y = resPoints[resPoints.Count - 1].Y;

				// вычисление значения прозводной n-го порядка в данной точке
				double funcValue = FundFuncValue(equationCopy);

				double y = double.NaN;

				// поиск начальных значений (используя метод Эйлера)
				if (funcValues.Count < 3)
				{
					y = resPoints[resPoints.Count - 1].Y + step * funcValue;

					resPoints.Add(new DataPoint(x, y));
					funcValues.Add(funcValue);

					continue;
				}

				// вычисление оставшихся значений многошаговым методом

				double deltaY = funcValue - funcValues[funcValues.Count - 1];
				double deltaY2 = funcValue - 2 * funcValues[funcValues.Count - 1] + funcValues[funcValues.Count - 2];
				double deltaY3 = funcValue - 3 * funcValues[funcValues.Count - 1] + 3 * funcValues[funcValues.Count - 2] - funcValues[funcValues.Count - 3];

				y = resPoints[resPoints.Count - 1].Y + step * funcValue + (Math.Pow(step, 2) / 2) * deltaY + ((5 * Math.Pow(step, 3)) / 12) * deltaY2 + ((3 * Math.Pow(step, 4)) / 8) * deltaY3;

				funcValues.Add(funcValue);
				resPoints.Add(new DataPoint(x, y));
			}


			// преобразование значений производной n-го порядка в значения функции
			for (int i = 0; i < equation.Order - 1; i++)
			{
				// todo: сохранение значений функции вышестоящей степени
				funcValues = resPoints.Select(p => p.Y).ToList();

				for (int j = 1; j < resPoints.Count; j++)
				{
					// вычисление значения прозводной n-го порядка в данной точке
					double funcValue = resPoints[j].Y;

					double y = double.NaN;

					// поиск начальных значений (используя метод Эйлера)
					if (j <= 3)
					{
						y = resPoints[j - 1].Y + step * resPoints[j].Y;

						resPoints[j] = new DataPoint(resPoints[j].X, y);

						continue;
					}

					// вычисление оставшихся значений многошаговым методом

					double deltaY = funcValue - funcValues[j-1];
					double deltaY2 = funcValue - 2 * funcValues[j - 1] + funcValues[j - 2];
					double deltaY3 = funcValue - 3 * funcValues[j - 1] + 3 * funcValues[j - 2] - funcValues[j - 3];

					y = resPoints[j - 1].Y + step * funcValue + (Math.Pow(step, 2) / 2) * deltaY + ((5 * Math.Pow(step, 3)) / 12) * deltaY2 + ((3 * Math.Pow(step, 4)) / 8) * deltaY3;

					resPoints[j] = new DataPoint(resPoints[j].X, y);
				}
			}


			return resPoints;
		}
	}
}
