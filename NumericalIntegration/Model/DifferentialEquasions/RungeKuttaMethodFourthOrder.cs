﻿using NumericalIntegration.Model.OrdinaryDifferentialEquation;
using OxyPlot;
using System.Collections.Generic;

namespace NumericalIntegration.Model.NumericalMethods
{
	internal class RungeKuttaMethodFourthOrder : NumericalMethod
	{
		internal override List<DataPoint> Calculate(string func, IOrdinaryDifferentialEquation equation, double endX, double step)
		{
			// определение коэффициентов для метода Рунге-Кутте третьего порядка
			double[] aValues = new double[] { 0, 1 / 2d, 1 / 2d, 1 };
			double[,] bValues = new double[,]
			{
				{0,0,0 },
				{1/2d,0,0 },
				{0,1/2d,0 },
				{0,0,1/2d }
			};
			double[] cValues = new double[] { 1 / 6f, 1 / 3d, 1 / 3d, 1 / 6d };

			// порядок метода Ругне-Кутте
			int n = 4;

			List<DataPoint> resPoints = new List<DataPoint>();

			double startX = equation.InitialConditions[0].X;

			// установка начальной точки
			resPoints.Add(new DataPoint(startX, equation.InitialConditions[0].Y));

			// создание копии ограничений
			IOrdinaryDifferentialEquation equationCopy = equation.Clone() as IOrdinaryDifferentialEquation;

			for (double x = startX; x < endX; x += step)
			{
				double[] kValues = new double[n];

				for (int i = 0; i < n; i++)
				{
					equationCopy.InitialConditions[0].X = resPoints[resPoints.Count - 1].X + aValues[i] * step;

					double bSum = 0;
					for (int j = 0; j <= i - 1; j++)
						bSum += bValues[i, j] * kValues[j];
					equationCopy.InitialConditions[0].Y = resPoints[resPoints.Count - 1].Y + step * bSum;

					kValues[i] = step * FundFuncValue(equationCopy);
				}

				double deltaY = 0;
				for (int i = 0; i < n; i++)
				{
					deltaY += cValues[i] * kValues[i];
				}

				double y = resPoints[resPoints.Count - 1].Y + deltaY;

				resPoints.Add(new DataPoint(x, y));
			}
			for (int i = 0; i < equation.Order - 1; i++)
			{
				for (int j = 1; j < resPoints.Count; j++)
				{
					double[] kValues = new double[n];

					for (int k = 0; k < n; k++)
					{
						kValues[k] = step * resPoints[j].Y;
					}

					double deltaY = 0;
					for (int k = 0; k < n; k++)
					{
						deltaY += cValues[k] * kValues[k];
					}

					double y = resPoints[j - 1].Y + deltaY;

					resPoints[j] = new DataPoint(resPoints[j].X, y);
				}
			}

			return resPoints;
		}
	}
}
