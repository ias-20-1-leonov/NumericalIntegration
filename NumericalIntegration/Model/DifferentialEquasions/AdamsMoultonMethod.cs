﻿using NumericalIntegration.Model.OrdinaryDifferentialEquation;
using OxyPlot;
using System.Collections.Generic;
using System.Linq;

namespace NumericalIntegration.Model.NumericalMethods
{
	internal class AdamsMoultonMethod : NumericalMethod
	{
		internal override List<DataPoint> Calculate(string func, IOrdinaryDifferentialEquation equation, double endX, double step)
		{
			List<DataPoint> resPoints = new List<DataPoint>();

			double startX = equation.InitialConditions[0].X;

			List<double> funcValues = new List<double>();

			// установка начальной точки
			resPoints.Add(new DataPoint(startX, equation.InitialConditions[0].Y));

			// создание копии ограничений
			IOrdinaryDifferentialEquation equationCopy = equation.Clone() as IOrdinaryDifferentialEquation;

			for (double x = startX; x < endX; x += step)
			{
				// изменение ограничений
				equationCopy.InitialConditions[0].X = resPoints[resPoints.Count - 1].X;
				equationCopy.InitialConditions[0].Y = resPoints[resPoints.Count - 1].Y;

				// вычисление значения прозводной n-го порядка в данной точке
				double funcValue = FundFuncValue(equationCopy);

				double y;
				switch (resPoints.Count)
				{
					case 1: y = resPoints[resPoints.Count - 1].Y + step * funcValue; break;
					case 2: y = resPoints[resPoints.Count - 1].Y + (1 / 2d) * step * (funcValue + funcValues[funcValues.Count - 1]); break;
					case 3: y = resPoints[resPoints.Count - 1].Y + step * ((5 / 12d) * funcValue + (2 / 3d) * funcValues[funcValues.Count - 1] - (1 / 12d) * funcValues[funcValues.Count - 2]); break;
					case 4: y = resPoints[resPoints.Count - 1].Y + step * ((3 / 8d) * funcValue + (19 / 24d) * funcValues[funcValues.Count - 1] - (5 / 24d) * funcValues[funcValues.Count - 2] + (1 / 24d) * funcValues[funcValues.Count - 3]); break;
					default: y = resPoints[resPoints.Count - 1].Y + step * ((251 / 720d) * funcValue + (646 / 720d) * funcValues[funcValues.Count - 1] - (264 / 720d) * funcValues[funcValues.Count - 2] + (106 / 720d) * funcValues[funcValues.Count - 3] - (19 / 720d) * funcValues[funcValues.Count - 4]); break;
				};

				funcValues.Add(funcValue);
				resPoints.Add(new DataPoint(x, y));
			}

			// преобразование значений производной n-го порядка в значения функции
			for (int i = 0; i < equation.Order - 1; i++)
			{
				// сохранение значений функции вышестоящей степени
				funcValues = resPoints.Select(p => p.Y).ToList();

				for (int j = 1; j < resPoints.Count; j++)
				{
					// вычисление значения прозводной n-го порядка в данной точке
					double funcValue = resPoints[j].Y;

					double y;
					switch (j)
					{
						case 1: y = resPoints[j - 1].Y + step * funcValue; break;
						case 2: y = resPoints[j - 1].Y + (1 / 2d) * step * (funcValue + funcValues[j - 1]); break;
						case 3: y = resPoints[j - 1].Y + step * ((5 / 12d) * funcValue + (2 / 3d) * funcValues[j - 1] - (1 / 12d) * funcValues[j - 2]); break;
						case 4: y = resPoints[j - 1].Y + step * ((3 / 8d) * funcValue + (19 / 24d) * funcValues[j - 1] - (5 / 24d) * funcValues[j - 2] + (1 / 24d) * funcValues[j - 3]); break;
						default: y = resPoints[j - 1].Y + step * ((251 / 720d) * funcValue + (646 / 720d) * funcValues[j - 1] - (264 / 720d) * funcValues[j - 2] + (106 / 720d) * funcValues[j - 3] - (19 / 720d) * funcValues[j - 4]); break;
					};

					resPoints[j] = new DataPoint(resPoints[j].X, y);
				}
			}

			return resPoints;
		}
	}
}
