﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumericalIntegration.Model.OrdinaryDifferentialEquation
{
	internal class InitialCondition
	{
		IOrdinaryDifferentialEquation _container;

		public double X { get; set; }
		public double Y { get; set; }
		
		public int DerivativeOrder
		{
			get
			{
				if (_container.InitialConditions == null)
					return -1;

				int index = _container.InitialConditions.IndexOf(this);

				if (index<=0)
					return -1;

				return index;
			}
		}

		internal InitialCondition(IOrdinaryDifferentialEquation equation)
		{
			_container= equation;

			X = double.NaN;
			Y = double.NaN;
		}
	}
}
