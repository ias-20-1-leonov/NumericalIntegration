﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace NumericalIntegration.Model.OrdinaryDifferentialEquation
{
	internal interface IOrdinaryDifferentialEquation: INotifyPropertyChanged, ICloneable
	{
		string Func { get; set; }
		int Order { get; set; }
		ObservableCollection<InitialCondition> InitialConditions { get; }

		void SetOrder(int order);
	}
}
