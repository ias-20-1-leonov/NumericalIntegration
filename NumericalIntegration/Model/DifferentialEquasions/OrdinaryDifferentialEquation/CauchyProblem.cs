﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace NumericalIntegration.Model.OrdinaryDifferentialEquation
{
	internal class CauchyProblem : IOrdinaryDifferentialEquation
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private string _func;

		public string Func
		{
			get => _func;
			set
			{
				_func = value;
				OnPropertyChanged(nameof(Func));
			}
		}

		public ObservableCollection<InitialCondition> InitialConditions { get; }

		public int Order { get => InitialConditions.Count;	set => SetOrder(value); }

		public CauchyProblem()
		{
			Func = string.Empty;
			InitialConditions = new ObservableCollection<InitialCondition>()
			{
				new InitialCondition(this)
			};
		}

		public void SetOrder(int order)
		{
			if (order <= 0)
				throw new InvalidEnumArgumentException("");

			// удаление лишних ограничений
			if (Order > order)
			{
				while (Order != order)
				{
					InitialConditions.RemoveAt(Order - 1);
				}
			}

			// добавление ограничений 
			if (Order < order)
			{
				while (Order != order)
				{
					InitialConditions.Add(new InitialCondition(this));
				}
			}
		}

		protected void OnPropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public object Clone()
		{
			var problem = new CauchyProblem()
			{
				Func = this.Func
			};

			problem.InitialConditions[0].X = this.InitialConditions[0].X;
			problem.InitialConditions[0].Y = this.InitialConditions[0].Y;

			problem.SetOrder(this.Order);

			for (int i = 1; i<InitialConditions.Count;i++)
			{
				problem.InitialConditions[i].X = this.InitialConditions[i].X;
				problem.InitialConditions[i].Y = this.InitialConditions[i].Y;
			}

			return problem;
		}
	}
}
