﻿using NumericalIntegration.Model.OrdinaryDifferentialEquation;
using OxyPlot;
using System.Collections.Generic;
using System.Linq;

namespace NumericalIntegration.Model.NumericalMethods
{
	internal class EulerMethod : NumericalMethod
	{
		internal override List<DataPoint> Calculate(string func, IOrdinaryDifferentialEquation equation, double endX, double step)
		{
			List<DataPoint> resPoints = new List<DataPoint>();

			double startX = equation.InitialConditions[0].X;

			// установка начальной точки
			resPoints.Add(new DataPoint(startX, equation.InitialConditions[0].Y));

			// создание копии ограничений
			IOrdinaryDifferentialEquation equationCopy = equation.Clone() as IOrdinaryDifferentialEquation;

			for (double x = startX; x < endX; x += step)
			{
				// изменение ограничений
				equationCopy.InitialConditions[0].X = resPoints[resPoints.Count - 1].X;
				equationCopy.InitialConditions[0].Y = resPoints[resPoints.Count - 1].Y;

				// вычисление значения прозводной n-го порядка в данной точке
				var a = FundFuncValue(equationCopy);
				double y = resPoints[resPoints.Count - 1].Y + step * a;

				resPoints.Add(new DataPoint(x, y));
			}

			// преобразование значений производной n-го порядка в значения функции
			for (int i = 0; i<equation.Order -1; i++)
			{
				for (int j = 1; j<resPoints.Count; j++)
				{
					double y = resPoints[j - 1].Y + step * resPoints[j].Y;
					resPoints[j] = new DataPoint(resPoints[j].X, y);
				}
			}

			return resPoints;
		}
	}
}
