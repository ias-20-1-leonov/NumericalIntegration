﻿using NumericalIntegration.Model.OrdinaryDifferentialEquation;
//using org.mariuszgromada.math.mxparser;
using OxyPlot;
using System.Collections.Generic;
using System.Collections.ObjectModel;
//using Mathos.Parser;
using System;
using System.Text.RegularExpressions;
using Mathos.Parser;

namespace NumericalIntegration.Model.NumericalMethods
{
    internal abstract class NumericalMethod
    {
        internal abstract List<DataPoint> Calculate(string func, IOrdinaryDifferentialEquation equation, double endX, double step);

        protected double FundFuncValue(IOrdinaryDifferentialEquation equation)
        {
            Mathos.Parser.MathParser parser = new Mathos.Parser.MathParser();

            parser.LocalVariables.Add("x", equation.InitialConditions[0].X);

            for (int i = 0; i < equation.InitialConditions.Count; i++)
            {
                parser.LocalVariables.Add($"y{new string('\'', i)}", equation.InitialConditions[i].X);

            }

            return parser.Parse(equation.Func);

        }
    }
}
