﻿using NumericalIntegration.Model.OrdinaryDifferentialEquation;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumericalIntegration.Model.NumericalMethods
{
	internal class EulerMethodWithIterativeProcessing : NumericalMethod
	{
		internal override List<DataPoint> Calculate(string func, IOrdinaryDifferentialEquation equation, double endX, double step)
		{
			List<DataPoint> resPoints = new List<DataPoint>();

			// колличество итераций
			const int K = 3;

			double startX = equation.InitialConditions[0].X;

			// установка начальной точки
			resPoints.Add(new DataPoint(startX, equation.InitialConditions[0].Y));

			// создание копии ограничений
			IOrdinaryDifferentialEquation equationCopy = equation.Clone() as IOrdinaryDifferentialEquation;

			for (double x = startX; x < endX; x += step)
			{
				// вычисление начального приближения
				double approximateValue = resPoints[resPoints.Count - 1].Y + step * FundFuncValue(equationCopy);

				// вычисление значения функции в текущей точке
				double funcCurrentValue = FundFuncValue(equationCopy);

				double y = double.NaN;

				for (int i = 0; i < K; i++)
				{
					// изменение ограничений
					equationCopy.InitialConditions[0].X = resPoints[resPoints.Count - 1].X;
					equationCopy.InitialConditions[0].Y = approximateValue;

					// вычисление значения функции в следующей точке
					double funcNextValue = FundFuncValue(equationCopy);

					// вычисление уточненного значения
					y = resPoints[resPoints.Count - 1].Y + (step * (funcCurrentValue + funcNextValue) / 2);
				}

				resPoints.Add(new DataPoint(x, y));
			}

			// преобразование значений производной n-го порядка в значения функции
			for (int i = 0; i < equation.Order - 1; i++)
			{
				for (int j = 1; j < resPoints.Count; j++)
				{
					// вычисление начального приближения
					double approximateValue = resPoints[j - 1].Y + step * resPoints[j].Y;

					// вычисление значения функции в текущей точке
					double funcCurrentValue = resPoints[j].Y;

					double y = double.NaN;

					for (int k = 0; k < K; k++)
					{
						// вычисление значения функции в следующей точке
						double funcNextValue = resPoints[j].Y;

						// вычисление уточненного значения
						y = resPoints[j - 1].Y + (step * (funcCurrentValue + funcNextValue) / 2);
					}
					resPoints[j] = new DataPoint(resPoints[j].X, y);
				}
			}

			return resPoints;
		}
       
    }
}
