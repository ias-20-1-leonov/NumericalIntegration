﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumericalIntegration.Model.Differentiation
{
    internal class UnstableStepMethod : BaseDerivativeCalculator
    {
        public override List<DataPoint> GetDerivative(List<DataPoint> functionPoints, int order)
        {
            List<DataPoint> result = new List<DataPoint>();
            if (functionPoints.Count < 4) throw new Exception("Недостаточное количество точек");
            for (int i = 1; i < functionPoints.Count - 2; i++)
            {
                double h = functionPoints[i].X - functionPoints[i - 1].X;
                double y = (1d / (6 * h)) * (-2 * functionPoints[i - 1].Y - 3 * functionPoints[i].Y + 6 * functionPoints[i + 1].Y - functionPoints[i + 2].Y);
                result.Add(new DataPoint(functionPoints[i].X, y));
            }
            return order < 2 ? result : GetDerivative(result, order--);
        }
    }
}
