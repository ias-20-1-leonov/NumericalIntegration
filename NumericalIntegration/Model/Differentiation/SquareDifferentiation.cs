﻿using MathNet.Numerics.Distributions;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumericalIntegration.Model.Differentiation
{
    internal class SquareInterpolation : BaseDerivativeCalculator
    {
        public override List<DataPoint> GetDerivative(List<DataPoint> functionPoints, int order)
        {
            if (order >= 4) throw new Exception("невозможно посчтитать производные больше 3го порядка");
            List<DataPoint> derPoints = new List<DataPoint>();
            double h = functionPoints[1].X - functionPoints[0].X;

            double DerivativeValue(int index, int degree)
            {
                if (degree == 0)
                    return functionPoints[index].Y;

                return (DerivativeValue(index + 1, degree - 1) - DerivativeValue(index, degree - 1)) / h;
            }

            // конечная разность n-ого порядка
            // order - порядок
            double EndDifference(int index, int orderDif)
            {
                if (orderDif == 1)
                    return DerivativeValue(index + 1, order - 1) - DerivativeValue(index, order - 1);

                return EndDifference(index + 1, orderDif - 1) - EndDifference(index, orderDif - 1);
            }


            for (int i = 0; i < functionPoints.Count - order - 1; i++)
            {
                /*double delta = (functionPoints[i + 1].X - functionPoints[i].X) / 10;
                h = functionPoints[i + 1].X - functionPoints[i].X;
                for (double x = functionPoints[i].X; x < functionPoints[i+1].X; x+=delta)
                {
                    double q = (x - functionPoints[i].X) / h;
                    double deltaY = functionPoints[i + 1].Y - functionPoints[i].Y;


                    double y = (1 / h) * (deltaY + 
                        ((2 * q - 1) / 2) * Math.Pow(delta, 2) * functionPoints[i].Y -
                        ((3 * q * q - 6 * q + 2) / 6) * Math.Pow(delta, 3) * functionPoints[i].Y);
                    derPoints.Add(new DataPoint(x, y));
                }*/

                h = functionPoints[i + 1].X - functionPoints[i].X;
                double q = (functionPoints[i + 1].X - functionPoints[i].X) / h;
                double deltaY = functionPoints[i + 1].Y - functionPoints[i].Y;

                double x = (functionPoints[i].X + functionPoints[i + 1].X) / 2;

                double y = (1 / h) * (EndDifference(i, 1) + ((2 * q - 1) / 2) *EndDifference(i, 2) /*+ ((3 * q * q - 6 * q + 2) / 6) * EndDifference(i, 3)*/);
                derPoints.Add(new DataPoint(x, y));
            }

            if (order == 1) return derPoints;
            else return GetDerivative(derPoints, order - 1);
        }
    }


    internal class CubicInterpolation : BaseDerivativeCalculator
    {
        public override List<DataPoint> GetDerivative(List<DataPoint> functionPoints, int order)
        {
            List<DataPoint> derPoints = new List<DataPoint>();
            double h = functionPoints[1].X - functionPoints[0].X;

            double DerivativeValue(int index, int degree)
            {
                if (degree == 0)
                    return functionPoints[index].Y;

                return (DerivativeValue(index + 1, degree - 1) - DerivativeValue(index, degree - 1)) / h;
            }

            // конечная разность n-ого порядка
            // order - порядок
            double EndDifference(int index, int orderDif)
            {
                if (orderDif == 1)
                    return DerivativeValue(index + 1, order - 1) - DerivativeValue(index, order - 1);

                return EndDifference(index + 1, orderDif - 1) - EndDifference(index, orderDif - 1);
            }


            for (int i = 0; i < functionPoints.Count - order - 2; i++)
            {
                /*double delta = (functionPoints[i + 1].X - functionPoints[i].X) / 10;
                h = functionPoints[i + 1].X - functionPoints[i].X;
                for (double x = functionPoints[i].X; x < functionPoints[i+1].X; x+=delta)
                {
                    double q = (x - functionPoints[i].X) / h;
                    double deltaY = functionPoints[i + 1].Y - functionPoints[i].Y;


                    double y = (1 / h) * (deltaY + 
                        ((2 * q - 1) / 2) * Math.Pow(delta, 2) * functionPoints[i].Y -
                        ((3 * q * q - 6 * q + 2) / 6) * Math.Pow(delta, 3) * functionPoints[i].Y);
                    derPoints.Add(new DataPoint(x, y));
                }*/

                h = functionPoints[i + 1].X - functionPoints[i].X;
                double q = (functionPoints[i + 1].X - functionPoints[i].X) / h;
                double deltaY = functionPoints[i + 1].Y - functionPoints[i].Y;

                double x = (functionPoints[i].X + functionPoints[i + 1].X) / 2;

                double y = (1 / h) * (EndDifference(i, 1) + ((2 * q - 1) / 2) * EndDifference(i, 2) + ((3 * q * q - 6 * q + 2) / 6) * EndDifference(i, 3));
                derPoints.Add(new DataPoint(x, y));
            }

            if (order == 1) return derPoints;
            else return GetDerivative(derPoints, order - 1);
        }
    }

}
