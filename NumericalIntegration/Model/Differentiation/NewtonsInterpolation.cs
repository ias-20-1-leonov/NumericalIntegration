﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumericalIntegration.Model.Differentiation
{
    internal class NewtonsInterpolation : BaseDerivativeCalculator
    {
        public override List<DataPoint> GetDerivative(List<DataPoint> functionPoints, int order)
        {
            List<DataPoint> derPoints = new List<DataPoint>();
            double h = functionPoints[1].X - functionPoints[0].X;
            Dictionary<int, double> endDifferences= new Dictionary<int, double>();
            Dictionary<int, double> factorials= new Dictionary<int, double>();
            for (double x = functionPoints[0].X; x < functionPoints[functionPoints.Count - 1].X; x+=0.1)
            {
                double y = 0;
                double q = x - functionPoints[0].X;
                double deltaq = q + q / 1000;
                for (int i = 1; i < functionPoints.Count; i++)
                {
                    if (!endDifferences.ContainsKey(i)) endDifferences.Add(i, GetEndDifference(functionPoints, 0, i));
                    double endDiff = endDifferences[i];
                    double numerator1 = 1;
                    double numerator2 = 1;
                    for (int j = 0; j < i; j++)
                    {
                        numerator1 *= (q - j);
                        numerator2 *= (deltaq - j);
                    }
                    if (!factorials.ContainsKey(i)) factorials.Add(i, GetFactorial(i));
                    double factorial = factorials[i];
                    numerator1 = (numerator2 - numerator1) / (q / 1000);
                    y += (numerator1 * endDiff) / factorial;
                    
                }
                derPoints.Add(new DataPoint(x, y / h));
            }

            return order <= 1 ? derPoints : GetDerivative(functionPoints, order - 1);
        }

        private double GetFactorial(int n)
        {
            double res = 1;
            for (int i = 2; i <= n; i++)
            {
                res *= i;
            }
            return res;
        }
    }
}
