﻿using org.mariuszgromada.math.mxparser;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NumericalIntegration.Model.Differentiation
{
    abstract class BaseDerivativeCalculator
    {
        public abstract List<DataPoint> GetDerivative(List<DataPoint> functionPoints, int order);
        public static Func<double, double> GetDerivativeFunc(Func<double, double> function, int order = 1, double delta = 0.01)
        {
            if (order <= 0) throw new Exception("Wrong derivative order");
            if (order == 1) return (a) => (function(a + delta) - function(a)) / delta;
            return GetDerivativeFunc(GetDerivativeFunc(function), order - 1, delta);
        }

        protected double GetEndDifference(List<DataPoint> points, int pos, int order)
        {
            if (order <= 1) return points[pos + 1].Y - points[pos].Y;
            return GetEndDifference(points, pos + 1, order - 1) - GetEndDifference(points, pos, order - 1);

        }
    }
}
