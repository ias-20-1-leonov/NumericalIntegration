﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NumericalIntegration.Model.Differentiation
{
    internal class LinearInterpolation : BaseDerivativeCalculator
    {
        public override List<DataPoint> GetDerivative(List<DataPoint> functionPoints, int order = 1)
        {
            List<DataPoint> derPoints = new List<DataPoint>();
            for (int i = 0; i < functionPoints.Count - 1; i++)
            {
                double x = (functionPoints[i].X + functionPoints[i + 1].X) / 2;
                double delta = functionPoints[i].X - functionPoints[i + 1].X;
                double y = (functionPoints[i].Y - functionPoints[i + 1].Y) / delta;
                derPoints.Add(new DataPoint(x, y));
            }
            if (order == 1) return derPoints;
            else return GetDerivative(derPoints, order - 1);
        }
    }
}
