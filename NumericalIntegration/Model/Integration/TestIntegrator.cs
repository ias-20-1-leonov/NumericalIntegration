﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace NumericalIntegration.Model
{
    internal class TestIntegrator : Integrator
    {
        public override double Integrate(Func<double, double> function, double start, double end, double step)
        {
            double sum = 0;
            List<Point> points = new List<Point>();
            double diff = end - start;
            long countOfElements = (long)(diff / step);
            step = diff / countOfElements;
            for (double x = start; x < end; x += step)
            {
                double y = function(x);
                sum += y * step;
                points.Add(new Point(x, 0));
                points.Add(new Point(x, y));
                points.Add(new Point(x + step, y));
                points.Add(new Point(x + step, 0));
            }
            OnCalculated(points);
            return sum;
        }
    }
}
