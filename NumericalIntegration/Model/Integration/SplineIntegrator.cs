﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics;

namespace NumericalIntegration.Model
{
    internal class SplineIntegrator : Integrator
    {
        public override double Integrate(Func<double, double> function, double start, double end, double step)
        {
            long n = (long)((end - start) / step);
            step = (end - start) / n;
            double sum = 0;

            for (long i = 1; i < n; i++)
            {
                double s1 = step * (function(start + (i - 1) * step) + function(start + i * step));
                double s2 = Math.Pow(step, 3) * Differentiate.Derivative(function, start + step * i, 2);
                sum += (0.5 * s1) - ((1/12) * s2);
            }
            return sum;
        }
    }
}
