﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NumericalIntegration.Model
{
    public class CentralRectangleIntegrator : Integrator
    {
        public override double Integrate(Func<double, double> function, double start, double end, double step)
        {
            double res = 0;
            List<Point> points = new List<Point>();
            for (double x = start; x < end; x += step)
            {
                var avgX = x + step / 2;
                var y = function(avgX);
                res += y;
                points.Add(new Point(x, 0));
                points.Add(new Point(x, y));
                points.Add(new Point(x + step, y));
                points.Add(new Point(x + step, 0));
            }
            OnCalculated(points);
            return res * step;
        }
    }
}
