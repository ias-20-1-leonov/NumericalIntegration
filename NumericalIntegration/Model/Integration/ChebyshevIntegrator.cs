﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumericalIntegration.Model
{
    internal class ChebyshevIntegrator : Integrator
    {
        int n = 3;
        Dictionary<int, double[]> ti = new Dictionary<int, double[]>
        {
            { 2, new double[] {-0.577340, 0.577360 } },
            { 3, new double[] {-0.707207, 0, 0.707207 } },
            { 4, new double[] {-0.794653, -0.187591, 0.187591, 0.794653 }},
            { 5, new double[] {-0.832497, -0.374542, 0, 0.374542, 0.832497 } },
            { 6, new double[] { -0.866246, -0.422518, -0.266634, 0.266634, 0.422518, 0.866248 } },
            { 7, new double[] { -0.883861, -0.529656, -0.323913, 0, 0.323913, 0.529656, 0.883861 } },
            { 9, new double[] { -0.911579, -0.601029, -0.528761, -0.167905, 0, 0.167905, 0.528761, 0.601018, 0.911579 }}
        };
        public override double Integrate(Func<double, double> function, double start, double end, double step)
        {
            double sum = 0;
            double a = 2.0 / n;
            long steps = (long)((end - start) / step);
            double[] t = ti[n];
            for (int i = 0; i < n; i++)
            {
                double xi = ((start + end) / 2) + ((end - start) / 2) * t[i];
                sum += a * function(xi);
            }

            return ((end - start) / 2) * sum;
        }


    }
}
