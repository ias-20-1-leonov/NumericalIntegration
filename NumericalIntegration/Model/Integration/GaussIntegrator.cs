﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.AxHost;

namespace NumericalIntegration.Model
{
    internal class GaussIntegrator : Integrator
    {
        private double GetPolynomialRootValue(int i, int n)
        {
            double PolynomialRootValue(int k)
            {
                if (k == 0)
                    return Math.Cos((Math.PI * (4 * i - 1)) / (4 * n + 2));

                double b = PolynomialRootValue(k - 1);

                return b - (GetPolynomial(b, n) / GetPolynomialDerivative(b, n));
            }

            double t = PolynomialRootValue(n - 1);

            return t - (GetPolynomial(t, n) / GetPolynomialDerivative(t, n));
        }

        private double GetPolynomial(double t, int n)
        {
            if (n == 1)
                return t;
            if (n == 0)
                return 1;

            int k = n - 1;

            return (((2 * (n - 1)) + 1) * t * GetPolynomial(t, n - 1) / ((n - 1) + 1)) - (((n - 1) * GetPolynomial(t, n - 2)) / ((n - 1) + 1));
        }

        private double GetPolynomialDerivative(double t, int n)
        {
            var a = GetPolynomial(t, n - 1) - t * GetPolynomial(t, n);
            return (n / (1 - Math.Pow(t, 2))) * (GetPolynomial(t, n - 1) - t * GetPolynomial(t, n));
        }

        public override double Integrate(Func<double, double> function, double start, double end, double step)
        {
            const int n = 3;

            double sum = 0;

            for (int i = 1; i <= n; i++)
            {
                double t = GetPolynomialRootValue(i, n);
                double x = ((start + end) / 2) + ((start - end) / 2) * t;
                double a = 2 / ((1 - Math.Pow(t, 2)) * Math.Pow(GetPolynomialDerivative(t, n), 2));
                double f = function(x);
                sum += a * f;
            }
            return ((end - start) / 2) * sum;
        }
    }
}
