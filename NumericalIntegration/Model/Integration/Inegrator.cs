﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace NumericalIntegration.Model
{
    public abstract class Integrator
    {
        public abstract double Integrate(Func<double,double> function, double start, double end, double step);

        public delegate void PointsCalculated(List<Point> points);
        public event PointsCalculated Calculated;

        protected void OnCalculated(List<Point> points)
        {
            Calculated?.Invoke(points);
        }
    }

    

   

    

    
}
