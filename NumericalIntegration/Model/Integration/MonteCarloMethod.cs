﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumericalIntegration.Model
{
    internal class MonteCarloMethod : Integrator
    {
        public override double Integrate(Func<double, double> function, double start, double end, double step)
        {
            double interval = end - start;
            long pointsCount = (long)(1 / step);
            long positivePoints = 0;
            long negativePoints = 0;
            double maxY = double.MinValue;
            double minY = double.MaxValue;
            for (double i = start; i < end; i += step / 100)
            {
                if (function(i) > maxY)
                    maxY = function(i);
                if (function(i) < minY)
                    minY = function(i);
            }
            Random random = new Random();
            for (double i = 0; i < pointsCount; i++)
            {
                double x =  start + random.NextDouble() * interval;
                double y = minY + random.NextDouble() * (maxY - minY);
                if (Math.Abs(y) < Math.Abs(function(x)))
                {
                    if (y < 0) negativePoints++;
                    else positivePoints++;
                }
               if (pointsCount <= 1000) OnCalculated(new List<System.Windows.Point>() { new System.Windows.Point(x, y), new System.Windows.Point(x + 0.01, y + 0.01) });
            }

           
            return (maxY - minY) * interval * ((double)(positivePoints - negativePoints) / pointsCount);
        }
    }
}
