﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NumericalIntegration.Model
{
    internal class TrapezeIntegrator : Integrator
    {
        public override double Integrate(Func<double, double> function, double start, double end, double step)
        {
            double area = 0;
            List<Point> points= new List<Point>();

            double diff = end - start;
            long countOfElements = (long)(diff / step);
            step = diff / countOfElements;

            for (double x = start; x < end - step / countOfElements; x += step)
            {
                double y1 = function(x);
                double y2 = function(x + step);
                area += (y1 + y2) * step / 2;
                points.Add(new Point(x, 0));
                points.Add(new Point(x, y1));
                points.Add(new Point(x + step, y2));
                points.Add(new Point(x + step, 0));
            }

            OnCalculated(points);
            return area;
        }
    }
}
