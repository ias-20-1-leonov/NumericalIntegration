﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumericalIntegration.Model
{
    public class SimpsonIntegrator : Integrator
    {
        public override double Integrate(Func<double, double> function, double start, double end, double step)
        {
            double sum = 0;
            int n = (int)((end - start) / step);
            n = n % 2 == 0 ? n : n + 1;
            step = (end - start) / n;

            for (int i = 0; i < n; i++)
            {
                double x1 = start + i * step;
                double x2 = start + (i + 1) * step;

                sum += ((x2 - x1) / 6) * (function(x1) + 4 * function(0.5 * (x1 + x2)) + function(x2));
            }
            return sum;
        }
    }
}
