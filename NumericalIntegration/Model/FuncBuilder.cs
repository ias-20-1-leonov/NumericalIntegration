﻿using NumericalIntegration.Model.NumericalMethods;
using NumericalIntegration.Model.OrdinaryDifferentialEquation;
//using org.mariuszgromada.math.mxparser;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace NumericalIntegration.Model
{
	class FuncBuilder : INotifyPropertyChanged
	{
		double _endX;
		double _step;

		public event PropertyChangedEventHandler PropertyChanged;

		#region Propertys
		public PlotModel Model { get; }

		public double EndX
		{
			get => _endX;
			set
			{
				_endX = value;
			}
		}

		public double Step
		{
			get => _step;
			set
			{
				if (value > 0)
					_step = value;
			}
		}

		#endregion

		#region Constructor

		public FuncBuilder(double endX, double step)
		{
			_endX = endX;
			_step = step;

			var yAxis = new LinearAxis()
			{
				Key = "YAxis",
				Position = AxisPosition.Left,
				Maximum = endX,
				MinimumRange = 10,
				MaximumRange = 100,
				ExtraGridlines = new double[] { 0 },
				ExtraGridlineThickness = 2,
				ExtraGridlineColor = OxyColors.Black,
				MajorGridlineColor = OxyColors.LightGray,
				MajorGridlineStyle = LineStyle.Solid,
				MajorTickSize = 2
			};

			var xAxis = new LinearAxis()
			{
				Key = "XAxis",
				Position = AxisPosition.Bottom,
				Maximum = endX,
				MinimumRange = 10,
				MaximumRange = 100,
				ExtraGridlines = new double[] { 0 },
				ExtraGridlineThickness = 2,
				ExtraGridlineColor = OxyColors.Black,
				MajorGridlineColor = OxyColors.LightGray,
				MajorGridlineStyle = LineStyle.Solid,
				MajorTickSize = 2
			};

			Model = new PlotModel();
			Model.Axes.Clear();

			Model.Axes.Add(xAxis);
			Model.Axes.Add(yAxis);
		}

		#endregion

		#region Chart building

		// возвращает true в случае успешного построения графика
		public virtual bool TryBuildChart(string func, NumericalMethod method, IOrdinaryDifferentialEquation problem)
		{
			if (func == string.Empty || method == null || problem == null)
				return false;

			// очищение графика
			Model.Series.Clear();

			List<DataPoint> points;
			try
			{
				// получение точек посредством переданного метода
				points = method.Calculate(func, problem, EndX, Step);
			}
			catch (Exception)
			{
				return false;
			}

			// добавление точек на график
			FunctionSeries function = new FunctionSeries()
			{
				Color = OxyColors.Red,
				StrokeThickness = 3
			};
			function.Points.AddRange(points);
			Model.Series.Add(function);

			// обновление графика функции в UI-потоке
			Application.Current.Dispatcher.Invoke(() =>
			{
				Model.InvalidatePlot(true);
			});

			return true;
		}

		#endregion

		#region PropertyChanged

		private void OnPropertyChanged(string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
