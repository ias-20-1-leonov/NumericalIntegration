﻿using NumericalIntegration.Model;
using NumericalIntegration.Model.Differentiation;
using org.mariuszgromada.math.mxparser;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace NumericalIntegration.ViewModel
{
    internal class MainWindowViewModel : DependencyObject
    {
        private readonly PlotView plotViewIntegration;
        private readonly PlotView plotViewDifferentiation;
        

        private readonly LinearAxis xAxisIntegration;
        private readonly LinearAxis xAxisDifferentiation;
        private readonly LinearAxis xAxisDE;



        private readonly PlotModel plotModelIntegration;
        private readonly PlotModel plotModelDifferentiation;
        private readonly PlotModel plotModelDE;

        private int derivativeOrder { get; set; }
        private double dx;

        public int DerivativeOrder { get => derivativeOrder; set => derivativeOrder = value >= 1 ? value : 1; }
        public string FunctionDiff { get; set; }
        public string Function { get; set; }
        public double Start { get; set; }
        public double StartDiff { get; set; }
        public double End { get; set; }
        public double EndDiff { get; set; }
        public double Step { get; set; }
        public double Dx { get => dx; set => dx = value > 0 ? value : 0.01; }
        public int[] EquasionOrders { get; set; }
        public int EquasionOrder { get; set; }


        #region visibility for different de orders

        public Visibility IsOrderGreater1
        {
            get => (Visibility)GetValue(IsOrderGreater1Property);
            set => SetValue(IsOrderGreater1Property, value);
        }

        public static readonly DependencyProperty IsOrderGreater1Property =
            DependencyProperty.Register("IsOrderGreater1", typeof(Visibility), typeof(MainWindowViewModel), new PropertyMetadata(Visibility.Collapsed));
        public Visibility IsOrderGreater2
        {
            get => (Visibility)GetValue(IsOrderGreater2Property);
            set => SetValue(IsOrderGreater2Property, value);
        }

        public static readonly DependencyProperty IsOrderGreater2Property =
            DependencyProperty.Register("IsOrderGreater2", typeof(Visibility), typeof(MainWindowViewModel), new PropertyMetadata(Visibility.Collapsed));

        public Visibility IsOrderGreater3
        {
            get => (Visibility)GetValue(IsOrderGreater3Property);
            set => SetValue(IsOrderGreater3Property, value);
        }

        public static readonly DependencyProperty IsOrderGreater3Property =
            DependencyProperty.Register("IsOrderGreater3", typeof(Visibility), typeof(MainWindowViewModel), new PropertyMetadata(Visibility.Collapsed));
        #endregion





        public MainWindowViewModel(PlotView pvIntegration, PlotView pvDiff)
        {
            plotModelIntegration = new PlotModel();
            plotModelDifferentiation = new PlotModel();
            plotModelDE = new PlotModel();

            plotViewDifferentiation = pvDiff;
            plotViewDifferentiation.Model = plotModelDifferentiation;


            plotViewIntegration = pvIntegration;
            plotViewIntegration.Model = plotModelIntegration;

            //plotViewDE = pvDE;
            //plotViewDE.Model = plotModelDE;

            //visibility change
            /*DEDegreeCB.SelectionChanged += (a, b) =>
            {
                IsOrderGreater1 = (int)DEDegreeCB.SelectedValue > 1 ? Visibility.Visible : Visibility.Collapsed;
                IsOrderGreater2 = (int)DEDegreeCB.SelectedValue > 2 ? Visibility.Visible : Visibility.Collapsed;
                IsOrderGreater3 = (int)DEDegreeCB.SelectedValue > 3 ? Visibility.Visible : Visibility.Collapsed;
            };*/

            #region integrationGrid
            plotModelIntegration.Axes.Add(new LinearAxis()
            {
                Key = "YAxis",
                Position = AxisPosition.Left,
                Minimum = plotModelIntegration.PlotArea.Bottom,
                Maximum = plotModelIntegration.PlotArea.Top,
                MinimumRange = 0.5,
                MaximumRange = 100,
                ExtraGridlines = new double[] { 0 },
                ExtraGridlineThickness = 2,
                ExtraGridlineColor = OxyColors.Black,
                MajorGridlineColor = OxyColors.LightGray,
                MajorGridlineStyle = LineStyle.Solid,
                MajorTickSize = 2
            });

            // ось X
            xAxisIntegration = new LinearAxis()
            {
                Key = "XAxis",
                Position = AxisPosition.Bottom,
                Minimum = plotModelIntegration.PlotArea.Left,
                Maximum = plotModelIntegration.PlotArea.Right,
                MinimumRange = 0.5,
                MaximumRange = 100,
                ExtraGridlines = new double[] { 0 },
                ExtraGridlineThickness = 2,
                ExtraGridlineColor = OxyColors.Black,
                MajorGridlineColor = OxyColors.LightGray,
                MajorGridlineStyle = LineStyle.Solid,
                MajorTickSize = 2
            };
            plotModelIntegration.Axes.Add(xAxisIntegration);

            xAxisIntegration.AxisChanged += (a, b) =>
            {
                foreach (OxyPlot.Series.Series item in plotModelIntegration.Series)
                {
                    if (item is EditableFunctionSeries)
                    {
                        (item as EditableFunctionSeries).SetXBounds(xAxisIntegration.ActualMinimum, xAxisIntegration.ActualMaximum);
                    }
                }
            };
            #endregion
            #region DifferentiationGrid
            plotModelDifferentiation.Axes.Add(new LinearAxis()
            {
                Key = "YAxis",
                Position = AxisPosition.Left,
                Minimum = plotModelIntegration.PlotArea.Bottom,
                Maximum = plotModelIntegration.PlotArea.Top,
                MinimumRange = 0.05,
                MaximumRange = 100,
                ExtraGridlines = new double[] { 0 },
                ExtraGridlineThickness = 2,
                ExtraGridlineColor = OxyColors.Black,
                MajorGridlineColor = OxyColors.LightGray,
                MajorGridlineStyle = LineStyle.Solid,
                MajorTickSize = 2
            });

            // ось X
            xAxisDifferentiation = new LinearAxis()
            {
                Key = "XAxis",
                Position = AxisPosition.Bottom,
                Minimum = plotModelIntegration.PlotArea.Left,
                Maximum = plotModelIntegration.PlotArea.Right,
                MinimumRange = 0.05,
                MaximumRange = 100,
                ExtraGridlines = new double[] { 0 },
                ExtraGridlineThickness = 2,
                ExtraGridlineColor = OxyColors.Black,
                MajorGridlineColor = OxyColors.LightGray,
                MajorGridlineStyle = LineStyle.Solid,
                MajorTickSize = 2
            };
            plotModelDifferentiation.Axes.Add(xAxisDifferentiation);

            xAxisDifferentiation.AxisChanged += (a, b) =>
            {
                foreach (OxyPlot.Series.Series item in plotModelDifferentiation.Series)
                {
                    if (item is EditableFunctionSeries)
                    {
                        (item as EditableFunctionSeries).SetXBounds(xAxisDifferentiation.ActualMinimum, xAxisDifferentiation.ActualMaximum);
                    }
                }
            };

            #endregion
            #region DE grid

            plotModelDE.Axes.Add(new LinearAxis()
            {
                Key = "YAxis",
                Position = AxisPosition.Left,
                Minimum = plotModelDE.PlotArea.Bottom,
                Maximum = plotModelDE.PlotArea.Top,
                MinimumRange = 0.05,
                MaximumRange = 100,
                ExtraGridlines = new double[] { 0 },
                ExtraGridlineThickness = 2,
                ExtraGridlineColor = OxyColors.Black,
                MajorGridlineColor = OxyColors.LightGray,
                MajorGridlineStyle = LineStyle.Solid,
                MajorTickSize = 2
            });

            // ось X
            xAxisDE = new LinearAxis()
            {
                Key = "XAxis",
                Position = AxisPosition.Bottom,
                Minimum = plotModelDE.PlotArea.Left,
                Maximum = plotModelDE.PlotArea.Right,
                MinimumRange = 0.05,
                MaximumRange = 100,
                ExtraGridlines = new double[] { 0 },
                ExtraGridlineThickness = 2,
                ExtraGridlineColor = OxyColors.Black,
                MajorGridlineColor = OxyColors.LightGray,
                MajorGridlineStyle = LineStyle.Solid,
                MajorTickSize = 2
            };
            plotModelDE.Axes.Add(xAxisDE);

            xAxisDE.AxisChanged += (a, b) =>
            {
                foreach (OxyPlot.Series.Series item in plotModelDifferentiation.Series)
                {
                    if (item is EditableFunctionSeries)
                    {
                        (item as EditableFunctionSeries).SetXBounds(xAxisDE.ActualMinimum, xAxisDE.ActualMaximum);
                    }
                }
            };
            #endregion

            plotViewIntegration.Model = null;
            plotViewIntegration.Model = plotModelIntegration;

            IntegrationMethodsList = new Dictionary<string, Integrator>();

            #region integration methods
            Integrator RrectangleIntegrator = new RightRectangleIntegrator();
            RrectangleIntegrator.Calculated += ShowIntegrationLines;
            IntegrationMethodsList.Add("right rectangle", RrectangleIntegrator);

            Integrator LrectangleIntegrator = new LeftRectangleIntegrator();
            LrectangleIntegrator.Calculated += ShowIntegrationLines;
            IntegrationMethodsList.Add("left rectangle", LrectangleIntegrator);

            Integrator CrectangleIntegrator = new CentralRectangleIntegrator();
            CrectangleIntegrator.Calculated += ShowIntegrationLines;
            IntegrationMethodsList.Add("central rectangle", CrectangleIntegrator);

            Integrator monteCarloIntegrator = new MonteCarloMethod();
            monteCarloIntegrator.Calculated += ShowIntegrationLines;
            IntegrationMethodsList.Add("MonteCarlo", monteCarloIntegrator);

            Integrator trapezeIntegartor = new TrapezeIntegrator();
            trapezeIntegartor.Calculated += ShowIntegrationLines;
            IntegrationMethodsList.Add("trapeze", trapezeIntegartor);

            Integrator simpsonIntegartor = new SimpsonIntegrator();
            simpsonIntegartor.Calculated += ShowIntegrationLines;
            IntegrationMethodsList.Add("simpson", simpsonIntegartor);

            Integrator splineIntegrator = new SplineIntegrator();
            splineIntegrator.Calculated += ShowIntegrationLines;
            IntegrationMethodsList.Add("spline", splineIntegrator);

            Integrator gaussIntegartor = new GaussIntegrator();
            gaussIntegartor.Calculated += ShowIntegrationLines;
            IntegrationMethodsList.Add("gauss", gaussIntegartor);

            Integrator chebyshevIntegrator = new ChebyshevIntegrator();
            chebyshevIntegrator.Calculated += ShowIntegrationLines;
            IntegrationMethodsList.Add("chebyshev", chebyshevIntegrator);
            #endregion

            #region differentiationMethods
            DifferentiationMethodsList = new Dictionary<string, BaseDerivativeCalculator>();

            BaseDerivativeCalculator baseFunc = new LinearInterpolation();
            DifferentiationMethodsList.Add("по определению производной", baseFunc);

            BaseDerivativeCalculator linearCalc = new LinearInterpolation();
            DifferentiationMethodsList.Add("линейная интерполяция", linearCalc);

            BaseDerivativeCalculator squareDer = new SquareInterpolation();
            DifferentiationMethodsList.Add("квадратная интерполяция", squareDer);

            BaseDerivativeCalculator cubicDer = new CubicInterpolation();
            DifferentiationMethodsList.Add("кубическая интерполяция", cubicDer);

            BaseDerivativeCalculator newtonDer = new NewtonsInterpolation();
            DifferentiationMethodsList.Add("интерполяция ньютона", newtonDer);

            BaseDerivativeCalculator ubstableStep = new UnstableStepMethod();
            DifferentiationMethodsList.Add("непостоянного шага", ubstableStep);

            #endregion
            FunctionDiff = "sin(x)";
            DerivativeOrder = 1;
            Function = "sin(x)";
            Start = -10;
            StartDiff = -10;
            End = 10;
            EndDiff = 10;
            Step = 0.5;
            Dx = 0.01;
            EquasionOrder = 1;

            EquasionOrders = new int[] { 1, 2, 3, 4 };
        }


        //сделано через свойство зависимости для отображения изменений
        public string Result
        {
            get => (string)GetValue(ResultProperty);
            set => SetValue(ResultProperty, value);
        }

        // Using a DependencyProperty as the backing store for Result.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ResultProperty =
            DependencyProperty.Register("Result", typeof(string), typeof(MainWindowViewModel), new PropertyMetadata("0"));

        //список сопоставления методов и их названий
        public Dictionary<string, Integrator> IntegrationMethodsList { get; set; }

        public List<string> Methods => IntegrationMethodsList.Select(e => e.Key).ToList();

        public Dictionary<string, BaseDerivativeCalculator> DifferentiationMethodsList { get; set; }
        public List<string> MethodsDiff => DifferentiationMethodsList.Select(e => e.Key).ToList();

        private void ShowIntegrationLines(List<Point> points)
        {
            LineSeries ls = new LineSeries();
            foreach (Point point in points)
            {
                ls.Points.Add(new DataPoint(point.X, point.Y));
            }
            ls.StrokeThickness = 1;
            ls.Color = OxyColors.BlueViolet;

            plotViewIntegration.Model = null;
            plotModelIntegration.Series.Add(ls);
            plotViewIntegration.Model = plotModelIntegration;

        }
        public RelayCommand FindIntegrallCommand => new RelayCommand((obj) =>
        {
            Function f = new Function($"f(x) = {Function}");


            /* double fx = f.calculate(Math.PI / 3);
             double dfx = Differentiate.DerivativeFunc((x) => f.calculate(x), 1).Invoke(Math.PI / 3);
             double ddfx = Differentiate.DerivativeFunc((x) => f.calculate(x), 3).Invoke(Math.PI / 3);*/


            double func(double x) => f.calculate(x);


            plotModelIntegration.Series.Clear();
            //подстчет результата оп заданным параметрам
            try
            {
                if (Start == End || Start > End)
                {
                    throw new ArgumentException("Неверные границы интегрирования");
                }

                global::OxyPlot.Series.FunctionSeries seriesForIntegration = new FunctionSeries(func, Start, End, 0.1, Function)
                {
                    StrokeThickness = 3,
                    Color = OxyColors.Red
                };
                plotModelIntegration.Series.Add(seriesForIntegration);

                if (obj is null)
                {
                    throw new Exception();
                }

                Result = IntegrationMethodsList[obj.ToString()].Integrate(func, Start, End, Step).ToString();
            }
            catch (ArgumentException e)
            {
                _ = MessageBox.Show(e.Message);
                return;
            }
            catch (Exception)
            {
                _ = MessageBox.Show("Метод не выбран!");
                return;
            }
            global::NumericalIntegration.ViewModel.EditableFunctionSeries series = new EditableFunctionSeries(func, xAxisIntegration.ActualMinimum, xAxisIntegration.ActualMaximum, 0.1, Function)
            {
                StrokeThickness = 1,
                Color = OxyColors.BlueViolet,
                LineStyle = LineStyle.Dot
            };

            plotModelIntegration.Series.Add(series);

            plotViewIntegration.Model = null;
            plotViewIntegration.Model = plotModelIntegration;
        });


        public RelayCommand ShowDerivativeFunc => new RelayCommand((obj) =>
        {
            Function f = new Function($"f(x) = {FunctionDiff}");

            double func(double x) => f.calculate(x);

            plotModelDifferentiation.Series.Clear();
            //подстчет результата оп заданным параметрам

            EditableFunctionSeries seriesMain = new EditableFunctionSeries(func, xAxisDifferentiation.ActualMinimum, xAxisDifferentiation.ActualMaximum, 0.1, FunctionDiff)
            {
                StrokeThickness = 1,
                Color = OxyColors.Black,
                LineStyle = LineStyle.Solid
            };

            LineSeries seriesDiff = null;

            if (DifferentiationMethodsList[obj.ToString()] == DifferentiationMethodsList.First().Value)
            {
                Func<double, double> derivativeFunc = BaseDerivativeCalculator.GetDerivativeFunc(func, DerivativeOrder);

                seriesDiff = new EditableFunctionSeries(derivativeFunc, xAxisDifferentiation.ActualMinimum, xAxisDifferentiation.ActualMaximum, dx, "(" + FunctionDiff + ")'");
            }
            else
            {
                seriesDiff = new LineSeries();
                List<DataPoint> seriesPoints = new List<DataPoint>();
                for (double i = StartDiff; i < EndDiff; i += Dx)
                {
                    seriesPoints.Add(new DataPoint(i, func(i)));
                }
                try
                {
                    seriesDiff.Points.AddRange(DifferentiationMethodsList[obj.ToString()].GetDerivative(seriesPoints, DerivativeOrder));

                }
                catch (Exception ex)
                {
                    _ = MessageBox.Show(ex.Message);
                }
            }

            seriesDiff.StrokeThickness = 3;
            seriesDiff.Color = OxyColors.BlueViolet;
            seriesDiff.LineStyle = LineStyle.Dot;
            seriesDiff.Title = $"f'(x) by {obj}";

            plotModelDifferentiation.Series.Add(seriesMain);
            plotModelDifferentiation.Series.Add(seriesDiff);

            plotViewDifferentiation.Model = null;
            plotViewDifferentiation.Model = plotModelDifferentiation;

        });

    }

    public class EditableFunctionSeries : FunctionSeries
    {

        public EditableFunctionSeries(Func<double, double> f, double x0, double x1, double dx, string title = null) : base(f, x0, x1, dx, title)
        {
            this.f = f;

            this.dx = dx;
        }

        //класс для редактирования границ отрисовки графика, нужен для добавления/удаления точек графика для отобрадении на экране
        public EditableFunctionSeries(Func<double, double> fx, Func<double, double> fy, double t0, double t1, double dt, string title = null) : base(fx, fy, t0, t1, dt, title)
        {

        }


        public Func<double, double> f { get; private set; }


        private readonly double dx;


        private void SetMinX(double value)
        {
            if (f != null)
            {
                if (value < MinX)
                {
                    for (double x = value; x < MinX; x += dx)
                    {
                        Points.Add(new DataPoint(x, f(x)));
                    }
                }
                else
                {
                    for (int i = 0; i < Points.Count; i++)
                    {
                        if (Points[i].X < value)
                        {
                            Points.RemoveAt(i);
                        }
                    }
                }
            }



            MinX = value;

        }

        private void SetMaxX(double value)
        {
            if (f != null)
            {
                if (value > MaxX)
                {
                    for (double x = MaxX; x < value; x += dx)
                    {
                        Points.Add(new DataPoint(x, f(x)));
                    }
                }
                else
                {
                    for (int i = 0; i < Points.Count; i++)
                    {
                        if (Points[i].X > value)
                        {
                            Points.RemoveAt(i);
                        }
                    }
                }
            }
            MaxX = value;
        }

        public void SetXBounds(double minX, double maxX)
        {
            SetMinX(minX);
            SetMaxX(maxX);

            if (f != null)
            {
                Points.Sort((a, b) =>
                {
                    if (a.X == b.X)
                    {
                        return 0;
                    }

                    return a.X > b.X ? 1 : -1;
                });
            }
        }

    }
}
