﻿using NumericalIntegration.Model;
using NumericalIntegration.Model.NumericalMethods;
using NumericalIntegration.Model.OrdinaryDifferentialEquation;
using OxyPlot;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Timers;

namespace NumericalIntegration.ViewModel
{
	class DifferentialEquasionsViewModel : INotifyPropertyChanged
	{
		FuncBuilder _funcBuilder;
		Timer _buildFuncTimer;
		double _result;
		IOrdinaryDifferentialEquation _equation;

		public event PropertyChangedEventHandler PropertyChanged;

		#region Propertys
		public PlotModel PlotModel { get => _funcBuilder.Model; }
		public string FuncStr { get => _equation.Func; set => _equation.Func = value; }
		public double Result
		{
			get => _result;
			set
			{
				_result = value;
				OnPropertyChanged(nameof(Result));
			}
		}

		public double EndX
		{
			get => _funcBuilder.EndX;
			set
			{
				_funcBuilder.EndX = value;
			}
		}
		public double Step { get => _funcBuilder.Step; set => _funcBuilder.Step = value; }
		public string MethodName { get; set; }

		public ObservableCollection<InitialCondition> InitialCondition { get => _equation.InitialConditions; }

		public int Order
		{
			get => _equation.Order;
			set
			{
				_equation.Order = value;
				OnPropertyChanged(nameof(Order));
			}
		}
		#endregion

		#region Commands
		public RelayCommand Calculate
		{
			get => new RelayCommand(_=>
			{
				bool buildingSuccess;

				switch (MethodName)
				{
					case "EulerMethod": _funcBuilder.TryBuildChart(FuncStr, new EulerMethod(), _equation);
						break;
					case "EulerMethodWithRecalculation": _funcBuilder.TryBuildChart(FuncStr, new EulerMethodWithRecalculation(), _equation);
						break;
					case "EulerMethodWithIterativeProcessing": _funcBuilder.TryBuildChart(FuncStr, new EulerMethodWithIterativeProcessing(), _equation);
						break;
					case "ImprovedEulerMethod": _funcBuilder.TryBuildChart(FuncStr, new ImprovedEulerMethod(), _equation);
						break;
					case "RungeKuttaMethodFrictionOrder": _funcBuilder.TryBuildChart(FuncStr, new RungeKuttaMethodFrictionOrder(), _equation);
						break;
					case "RungeKuttaMethodFourthOrder": _funcBuilder.TryBuildChart(FuncStr, new RungeKuttaMethodFourthOrder(), _equation);
						break;
					case "AdamsMethod": _funcBuilder.TryBuildChart(FuncStr, new AdamsMethod(), _equation);
						break;
					case "AdamsBashfortMethod": _funcBuilder.TryBuildChart(FuncStr, new AdamsBashfortMethod(), _equation);
						break;
					case "AdamsMoultonMethod": _funcBuilder.TryBuildChart(FuncStr, new AdamsMoultonMethod(), _equation);
						break;
				}
			});
		}

		#endregion

		public DifferentialEquasionsViewModel()
		{
			_funcBuilder = new FuncBuilder(10, 1);
			_equation = new CauchyProblem();

			MethodName = "EulerMethod";
			FuncStr = "sin(x)";
			
		}

		#region Property changed
		private void OnPropertyChanged(string property = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
		}
		#endregion
	}
}
