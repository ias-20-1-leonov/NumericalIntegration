﻿using NumericalIntegration.Model.OrdinaryDifferentialEquation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace NumericalIntegration.Converters
{
	internal class OrdinaryDifferentialEquationConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			int order = (int)value;

			string view = "f(x, y, ";

			for (int i = 0; i < order - 1; i++)
			{
				view += $"y{new string('\'',i+1)}, ";
			}
			view += $"y{new string('\'', order)})=";

			return view;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
