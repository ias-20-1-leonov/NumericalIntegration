﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace NumericalIntegration.Converters
{
	internal class DerivativeConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			int count = (int)value;

			if (count <= 0)
				return string.Empty;

			return new string('\'', count);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string degrees = (string)value;

			return degrees.Where(d => d == '\'').Count();
		}
	}
}
