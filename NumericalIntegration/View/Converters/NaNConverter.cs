﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace NumericalIntegration.Converters
{
	internal class NaNConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			double v = (double)value;

			return (double.IsNaN(v)) ? string.Empty : v.ToString();
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string s = (string)value;
			double v;

			return (!double.TryParse(s, out v)) ? double.NaN : v;
		}
	}
}
