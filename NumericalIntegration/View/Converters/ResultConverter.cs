﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace NumericalIntegration.Converters
{
	class ResultConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			double v = (double)value;

			return (double.IsNaN(v))? string.Empty: v.ToString(CultureInfo.InvariantCulture);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string v = (string)value;

			return (v == string.Empty) ? double.NaN : double.Parse(v, CultureInfo.InvariantCulture);
		}
	}
}
